#include <Scanner.h>
#include <Key.h>

/**
 * Scanner Example - uses matrix scanner to scan keys
 */

  Scanner scanner(4,3);
  int last_code =0;

void setup() {
  Serial.begin(9600);
  scanner.addPortRow(12);
  scanner.addPortRow(11);
  scanner.addPortRow(10);
  scanner.addPortRow(9);
  scanner.addPortColumn(8);
  scanner.addPortColumn(6);
  scanner.addPortColumn(5);

  scanner.scan();
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  Key k = scanner.scan();
  // press key 10 and hold, LED_BUILTIN on
  if(k.code() == 10 && last_code != k.code() && k.held()) {
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.println(k.code());
    last_code = k.code();
  // press any key except 10, LED_BUILIN off
  } else if(k.code() != 10 && k.code() > 0 && last_code != k.code()) {
    digitalWrite(LED_BUILTIN, LOW);
    Serial.println(k.code());
    last_code = k.code();
  } else if(k.code() == 0 ) {
    last_code = 0;
  }
}
