#ifndef SCANNER_H
#define SCANNER_H

#define ROWS 8
#define COLS 8
#define FIRST_KEY_CODE 1
#define SCANS 10

#include "Key.h"

class Scanner
{
	public:
		Scanner(int, int);
		~Scanner();
		Key scan();
		void addPortRow(int);
		void addPortColumn(int);
	protected:

	private:
		int _rows;
		int _columns;
		Key _keys[ROWS][COLS];
		int _portsRow[ROWS];
		int _currentPortRow;
		int _portsColumn[COLS];
		int _currentPortColumn;
		
		void scanKeys();	
		bool scanKey(Key, int, int);
		void reset();
};
#endif
