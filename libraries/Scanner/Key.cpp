#include "Key.h"

Key::Key()
{
	_code = 0;
	_state = INACTIVE;
}	

Key::Key(int code)
{
	_code = code;
}

Key::~Key(){}	

int Key::code()
{
	return _code;
}

void Key::event(bool active, int time_millis)
{
	if(active)
	{
		int timeout = _last_event + HOLD_TIMEOUT;
		if(_state == ACTIVE)
		{
			if(time_millis > timeout) {
				_state = HELD;
				_last_event = time_millis;
			}
		} else if(_state == INACTIVE) {
			_state = ACTIVE;
			_last_event = time_millis;
		}
	} else {
		if(time_millis > _last_event + ACTIVE_TIMEOUT)
		{
			_state = INACTIVE;
		}
	}
	
}

bool Key::pressed()
{
	if(_state == ACTIVE)
	{
		return true;
	}
	return false;
}

bool Key::held()
{
	if(_state == HELD)
	{
		return true;
	}
	return false;
}

