#include "Scanner.h"
#include "Key.h"
#include "Arduino.h"

Scanner::Scanner(int rows, int columns)
{
	_rows = rows;
	_columns = columns;
	_currentPortRow = 0;

	int keyCode = FIRST_KEY_CODE;
	for(int row = 0; row < rows; row++) {
		for(int col = 0; col < columns; col++) {
			Key key(keyCode);
			_keys[row][col] = key;
			keyCode++;
		}
	}
}

Scanner::~Scanner(){}

Key Scanner::scan()
{
	Key key;
	scanKeys();
			for(int col=0; col < _columns; col++) 
			{
				for(int row=0; row < _rows; row++)
				{
					if(_keys[row][col].pressed() || _keys[row][col].held())
					{
						return _keys[row][col];
					}
				}
			}
	return key;
}

void Scanner::scanKeys()
{

		for(int i=0; i < SCANS; i++) {
			for(int col=0; col < _columns; col++)
			{
				for(int row=0; row < _rows; row++)
				{
					bool pressed = scanKey(_keys[row][col],row,col);
					_keys[row][col].event(pressed, millis());
				}
			}
		}
}

void Scanner::addPortRow(int port)
{
	_portsRow[_currentPortRow] = port;
	_currentPortRow++;
	pinMode(port, INPUT);
}

void Scanner::addPortColumn(int port)
{
	_portsColumn[_currentPortColumn] = port;
	digitalWrite(_currentPortColumn, HIGH);
	_currentPortColumn++;
	pinMode(port, OUTPUT);
}

bool Scanner::scanKey(Key key, int row, int col)
{
	digitalWrite(_portsColumn[col],LOW);
	int input = digitalRead(_portsRow[row]);
	digitalWrite(_portsColumn[col],HIGH);
	if(input == HIGH) {
		return false;
	} else {
		return true;
	}
}

