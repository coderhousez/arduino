#ifndef KEY_H
#define KEY_H

#define ACTIVE_TIMEOUT 200
#define HOLD_TIMEOUT 1000
#define INACTIVE 0
#define ACTIVE 1
#define HELD 2

class Key
{
	public:
		Key();
		Key(int);
		~Key();
		int code();
		// true if state is pressed
		bool pressed();
		bool held();
		// bool - pin state
		// int - time millis of event
		void event(bool, int);

	protected:

	private:
		int _code;
		int _state; // 0 = inactive, 1 = active
		int _last_event;
};

#endif

